#!/bin/bash

#Running the inspec tests for the deployed AWS resources
inspec exec https://github.com/ArunaLakmal/inspec-profile-aws-tc-k8s.git -t aws:// --input bucketname="${BUCKET}"